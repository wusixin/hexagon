# -*- coding: utf-8 -*-

import cv2 as cv
import numpy as np
import sys
from helper import * 

#mouse callback function
def show_hsv(event,x,y,flags,param):
    if event==cv.EVENT_LBUTTONDBLCLK:
        print("x=",x,"y=",y,"flags=",flags,"param=",param)
        print("BGR = ", frame[y,x])
        print("HSV = ", hsv[y,x])

# 创建图像与窗口并将窗口与回调函数绑定
frame = cv.imread(sys.argv[1])
hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
digitTemplates = loadDigitsTemplate("template.png")
digitSamples = []
cv.namedWindow('image')
cv.setMouseCallback('image',show_hsv)
image = frame.copy()
# 二值化红色和品红色区域
mask = getColorAreaMask(image, 0, 15) + getColorAreaMask(image, 165, 180)
polygon, area = findMaxPolygon(mask, 6)
if not polygon is None:
    # 如果面积够大, 识别成功
    if area > 10000:
        # 识别成功!!
        print("Found a BIG RED hexagon!, area = %d" % (area) )
        # 画粗线
        cv2.polylines(image, [polygon], True, (0, 255, 0), 4)

        # 黄色区域
        mask2 = getColorAreaMask(frame, 15, 45) # 黄色

        # 六边形内部的黄色区域
        temp_im = np.zeros(frame.shape[:2], dtype="uint8")  # 获取图像的维度: (h,w)=iamge.shape[:2]
        polygon_mask = cv2.fillPoly(temp_im, [polygon], 1)
        digitMask = mask2 * polygon_mask

        # 调试：显示出来
        [x,y,w,h] = cv2.boundingRect(polygon)
        subImage = digitMask[y:y+h,x:x+w]
        cv2.imshow("digitMask", subImage)

        c, area = findMaxContour(digitMask)
        if not area is None and area > 180: # 面积太小也许找丢了区域
            cv2.drawContours(image, c, -1, (0,255,0), 2)
            digitSamples.append(c)
            digit = guessDigit(digitSamples, digitTemplates)
            digitSamples = []
            print("RESULT:", digit)
    else:
        # 画细线
        cv2.polylines(image, [polygon], True, (0, 255, 0), 1)

while(1):
    cv.imshow('image',image)
    if cv.waitKey(20)&0xFF==27:
        break

cv.destroyAllWindows()