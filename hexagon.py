# -*- coding: utf-8 -*-
# 导入所需模块
import cv2
import numpy as np
import socket, select
import time
import sys, getopt, signal
from helper import *

bExitFlag = False
camara = None
digitTemplates = []
digitSamples = []

def detect_once(mode):
    """
    检测一次
    """
    global digitSamples, digitTemplates, camera
    result = None

    frame = getFrameCenter(camera, 424, 240) # frame 原始帧
    # frame = getFrameCenter(camera, 1280, 720) # frame 原始帧
    image = frame.copy()
    # 二值化红色和品红色区域
    mask = getColorAreaMask(image, 0, 15) + getColorAreaMask(image, 165, 180)

    polygon, area = findMaxPolygon(mask, 6)
    if not polygon is None:
        # 如果面积够大, 识别成功
        if area > 10000:
            # 识别成功!!
            print("Found a BIG RED hexagon!, area = %d" % (area) )
            if mode == 0:
                # 画粗线
                cv2.polylines(image, [polygon], True, (0, 255, 0), 4)

            # 黄色区域
            mask2 = getColorAreaMask(frame, 15, 45) # 黄色

            # 六边形内部的黄色区域
            temp_im = np.zeros(frame.shape[:2], dtype="uint8")  # 获取图像的维度: (h,w)=iamge.shape[:2]
            polygon_mask = cv2.fillPoly(temp_im, [polygon], 1)
            digitMask = mask2 * polygon_mask

            # 调试：显示出来
            if mode == 0:
                [x,y,w,h] = cv2.boundingRect(polygon)
                subImage = digitMask[y:y+h,x:x+w]
                cv2.imshow("digitMask", subImage)

            c, area = findMaxContour(digitMask)
            if not area is None and area > 180: # 面积太小也许找丢了区域
                cv2.drawContours(image, c, -1, (0,255,0), 2)
                digitSamples.append(c)
                if len(digitSamples) == 10:
                    digit = guessDigit(digitSamples, digitTemplates)
                    digitSamples = []
                    print("RESULT:", digit)
                    result = digit
        else:
            if mode == 0:
                # 画细线
                cv2.polylines(image, [polygon], True, (0, 255, 0), 1)

    return frame, image, result


def initCamera(zoomLevel):
    global camera
    # 打开摄像头
    camera = cv2.VideoCapture(0)
    # 三种分辨率 424x240, 960x540, 1280x720
    frameSizes = [[640,480], [960,540], [1280,720]]
    # 选取其中一个调节数码变焦
    camera.set(3, frameSizes[zoomLevel][0])
    camera.set(4, frameSizes[zoomLevel][1])

# GUI 方式启动时的主循环
def GUILoop():
    global bExitFlag
    bExitFlag = False

    while not bExitFlag:
        frame, image, result = detect_once(0)

        if result:
            cv2.putText(image, str(result), (10,80), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 255, 0), 2)
        # 可选的, 显示出这幅图像
        # cv2.imshow("BIG RED hexagon detection, ESC to quit", cv2.resize(image,(640,360)))
        cv2.imshow("BIG RED hexagon detection, ESC to quit", image)
        
        # ESC键退出
        k = cv2.waitKey(200) & 0xFF
        if k == 27:
            bExitFlag = True
        elif k == 32:
            cv2.imwrite(str(int(time.time())) + ".jpg", frame)

# 服务方式启动时的主循环
def ServiceLoop():
    global bExitFlag
    bExitFlag = False
    bSwitchON = False

    # 用UDP接受送其他进程来的启动信号
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('127.0.0.1', 9999))
    s.setblocking(0)
    print('Bind UDP on 9999...')
    inputs = [s]
    outputs = []
    client = None

    while not bExitFlag:
        readable, writable, exceptional = select.select(inputs, outputs, inputs, 0)
        if s in readable:
            try:
                buffer, client = s.recvfrom(1024)
                cmd = buffer.decode()
                if cmd == "ON":
                    bSwitchON = True
                    print("Got [ON] command, start detect ...")
                elif cmd == "OFF":
                    print("Got [OFF] command, stop detect ...")
                    bSwitchON = False
                    digitSamples = []
                else:
                    pass
            except:
                print("socket error, peer close? stop detect ...")
                bSwitchON = False
                digitSamples = []

        if bSwitchON:
            frame, image, result = detect_once(1)
            if not result is None:
                print("Got detect result [", result, "], send to client" , client)
                s.sendto(str(result).encode(), client)

        time.sleep(0.2)
    
    s.close()

# 信号相应
def sigint_handler(signum, frame):
  global bExitFlag
  bExitFlag = True
  print('catched interrupt signal!')

# 帮助信息
def usage():
    print("""
    识别大红六边形, 找出内部的0-2数字。
    %s [-z zoomLevel] [-s] [-h]
        -z 摄像头缩放级别， 0-2
        -s 以服务方式启动， 默认以GUI方式启动
        -h 显示本帮助信息
    """ % (sys.argv[0]))


# 主入口
if __name__ == '__main__':
    # 解析命令行参数
    opts, args = getopt.getopt(sys.argv[1:], "hsz:")
    mode = 0
    zoomLevel = 2
    output_file=""
    for op, value in opts:
        if op == "-z":
            zoomLevel = int(value)
        if op == "-s":
            mode = 1
        elif op == "-h":
            usage()
            sys.exit()

    # 加载需要识别的数字模型
    digitTemplates = loadDigitsTemplate("template.png")
    digitSamples = []

    # 初始化摄像头
    initCamera(zoomLevel)

    if mode == 0:
        GUILoop()
    else:
        # 退出进程信号
        signal.signal(signal.SIGINT, sigint_handler)
        signal.signal(signal.SIGTERM, sigint_handler)
        ServiceLoop()
    
    # 释放资源
    camera.release()
    cv2.destroyAllWindows()


