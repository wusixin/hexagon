# 测试程序
import socket, select

# 模拟用户按下按键，用UDP发出命令
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
print("send [ON] command to server ...")
s.sendto('ON'.encode(),("127.0.0.1", 9999))
# s.setblocking(0)
print('wait result ...')

inputs = [s]
outputs = []
readable, writable, exceptional = select.select(inputs, outputs, inputs, 300)
if s in readable:
    result, sender = s.recvfrom(1024)
    print(result, sender)
    # 将结果通知蓝牙设备

print("send [OFF] command to server ...")
s.sendto('OFF'.encode(),("127.0.0.1", 9999))

