# hexagon
简单的OpenCV程序
探测视频中的红色六边形，并检查六边形中的黄色数字0-2

使用:
```
python3 hexagon.py
```
启动到测试（GUI）模式

```
python3 hexagon.py -s
```
启动到服务模式。
服务模式在9999端口收到UDP包"ON"才开始工作， 并将检测结果（0-2）回送到发出命令的UDP端口

# changelog
19-3-9: 从数字改为图形，提高识别率
19-3-9: 增加testimage.py, 用于测试图像色彩、亮度等引起的问题
  用法  python3 testimage.py xxx.jpg
  jpg文件可以在hexagon GUI模式中按空格键生成
