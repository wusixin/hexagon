# Copyright (C) XXX
# Author: Raymond Wu
# Contact: wusixin@126.com
# 辅助函数
import numpy as np
import cv2

# 返回图像中间的矩形区域, 用于数码变焦


def getFrameCenter(camera, width, height):
  """
  返回图像中间的矩形区域, 用于数码变焦
  camera : OpenCV VideoCapture 对象
  @return: 返回的图像
  """
  _, frame = camera.read()
  frameWidth = frame.shape[1]
  frameHeight = frame.shape[0]
  left = max(int((frameWidth - width) / 2), 0)
  right = min(left + width, frameWidth)
  top = max(int((frameHeight - height) / 2), 0)
  bottom = min(top + height, frameHeight)
  return frame[top: bottom, left: right, :]


# 按色度上下限获得一个颜色遮罩
def getColorAreaMask(image, lower, upper):
  """
  按色度上下限获得一个颜色遮罩
  image : 输入图像
  lower, upper : 需二值化截取的色度区域（HSV中的H, 从0-179范围）
  @return: 返回的二值化图像遮罩
  """
  # 进行高斯模糊
  # blurred = cv2.GaussianBlur(image, (11, 11), 0)
  blurred = image.copy()
  # 转换颜色空间到HSV
  hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
  # 定义遮罩颜色色的HSV阈值
  # lower_hsv = np.array([lower, 123, 100])
  lower_hsv = np.array([lower, 90, 90])
  upper_hsv = np.array([upper, 255, 255])
  # 对图片进行二值化处理
  mask = cv2.inRange(hsv, lower_hsv, upper_hsv)
  # 腐蚀操作，先腐蚀后膨胀以滤除噪声
  mask = cv2.erode(mask, None, iterations=2)
  # 膨胀操作，先腐蚀后膨胀以滤除噪声
  mask = cv2.dilate(mask, None, iterations=2)
  return mask


# 寻找面积最大的轮廓
def findMaxContour(bImage):
  """
  在二值化图像中寻找面积最大的轮廓
  bImage : 输入图像
  @return: 轮廓, 面积
  """
  # 寻找图中轮廓
  cnts = cv2.findContours(bImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
  # 如果存在至少一个轮廓则进行如下操作
  if len(cnts) > 0:
      # 找到面积最大的轮廓
      c = max(cnts, key=cv2.contourArea)
      # 求轮廓面积
      area = cv2.contourArea(c)
      return c, area
  return None, None

# 寻找面积最大的多边形轮廓


def findMaxPolygon(bImage, n):
  """
  在二值化图像中寻找面积最大的凸多边形轮廓
  bImage : 输入图像
  n : 几边形
  @return: 轮廓, 面积
  """
  # 寻找图中轮廓
  cnts = cv2.findContours(bImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
  match = []
  for cnt in cnts:
    epsilon = 0.05 * cv2.arcLength(cnt, True)
    approx = cv2.approxPolyDP(cnt, epsilon, True)
    if len(approx) == n and cv2.isContourConvex(approx):
      match.append(approx)

  # 如果存在至少一个所需多边形轮廓则进行如下操作
  if len(match) > 0:
    # 找到面积最大的多边形轮廓
    c = max(match, key=cv2.contourArea)
    # 求多边形面积
    area = cv2.contourArea(c)
    return c, area
  return None, None

# 加载数字轮廓模板


def loadDigitsTemplate(templateFile):
  """
  读取数字模板的轮廓
  templateFile : 文件名
  @return: 模板的轮廓数据
  """
  # 读入数字模板图像
  im = cv2.imread(templateFile)
  gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
  # 直接阈值化是对输入的单通道矩阵逐像素进行阈值分割。
  _, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)
  contours,hierarchy = cv2.findContours(binary,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
  # contours[len(contours)-1] 是全部区域,舍弃
  # print(contours)
  return contours[0:len(contours)-1]

# 用最简单的轮廓匹配猜数字
def guessDigit(samples, templates):
  """
  用最简单的轮廓匹配猜数字
  samples : 多次轮廓数据, 有一定猜错几率所以做多次计算,取总匹配度最高的
  templates : 模板的轮廓数据
  @return: 数字索引
  """
  weight = []
  for i in range(len(templates)):
    weight.append(0)
    for j in range(len(samples)):
        w = cv2.matchShapes(templates[i], samples[j], 1, 0.0)
        # print("temp %d sample %d, weight = %f" % (i,j,w) )
        weight[i] += w
  return weight.index(min(weight))
